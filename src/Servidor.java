
/**
 * Autor: Marciano da Rocha
 * Ultima modificacao: 26/04/2018
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Servidor extends UnicastRemoteObject implements IServidor {

    private static final long serialVersionUID = 1L;
    private static final int PORTA_REGISTRO = 1099;
    private static final String NOME_SERVICO = "SERVIDOR_RMI";
    private int contadorTarefasExecutadas = 0;

    public Servidor() throws RemoteException {
        //Eh necessario ter um construtor aqui para
        //invocar o metodo construtor da superclasse
        super();
    }

    public static void main(String args[]) {

        //Gerenciador de Seguranca
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {
            //Criar o registry na porta especificada
            Registry registry = LocateRegistry.createRegistry(PORTA_REGISTRO);

            //Criar o servidor
            Servidor servidor = new Servidor();

            //Registrar o servidor
            registry.rebind(NOME_SERVICO, servidor);

            System.out.println("Servidor RMI em " + System.getenv("HOSTNAME") + " ativado.");

        } catch (Exception e) {

            System.out.println(e.getMessage());

        }//fim catch

    }//fim main

    public String executarTarefa(String args) throws RemoteException {

        String resultado = "";

        int recebido = Integer.parseInt(args);
        recebido += 10;

        //Se o cliente fornecer como argumento a string 1
        if (args.equals("1")) {
            resultado = "Primeira opcao. Valor recebido do cliente + 10: " + recebido;
        } else {
            resultado = "Segunda opcao. Valor recebido do cliente + 10: " + recebido;
        }
        System.out.println(resultado);
        contadorTarefasExecutadas++;
        return resultado;

    }//fim executarTarefa

    public String textoAoContrario(String args) throws RemoteException {
        StringBuilder str = new StringBuilder(args);
        String resultado = str.reverse().toString();
        System.out.println(resultado);
        return resultado;
    }

    public String dataHoraServidor() throws RemoteException {
        SimpleDateFormat sd = new SimpleDateFormat("hh:mm:ss - EEE, dd 'de' MMMMM 'de' yyyy");
        String resultado = sd.format(Calendar.getInstance().getTime());
        System.out.println(resultado);
        return resultado;
    }

    public String contadorTarefasExecutadas() throws RemoteException {
        String resultado = "Foi(ram) executado(as) " + this.contadorTarefasExecutadas + " tarefa(s).";
        System.out.println(resultado);
        return resultado;
    }

}//fim Servidor
