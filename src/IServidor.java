
/**
 * Autor: Marciano da Rocha
 * Ultima modificacao: 26/04/2018
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServidor extends Remote {

    public String executarTarefa(String argumentos) throws RemoteException;

    public String textoAoContrario(String args) throws RemoteException;

    public String dataHoraServidor() throws RemoteException;

    public String contadorTarefasExecutadas() throws RemoteException;

}//fim interface
