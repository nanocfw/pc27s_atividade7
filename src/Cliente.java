
/**
 * Autor: Marciano da Rocha
 * Ultima modificacao: 26/04/2018
 */
import java.rmi.Naming;

public class Cliente {

    private static final String NOME_SERVICO = "SERVIDOR_RMI";

    public Cliente(String h, String arg1, String arg2) {

        System.out.println(executarTarefaRemota(h, arg1));
        System.out.println(textoAoContrarioRemoto(h, arg2));
        System.out.println(dataHoraServidor(h));
        System.out.println(contadorTarefasExecutadasRemotamente(h));
    }//fim construtor	

    public static void main(String args[]) {

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        new Cliente(args[0], args[1], args[2]);

    }//fim main

    public String executarTarefaRemota(String host, String argumentos) {

        //A tarefa foi executada com sucesso quando o retorno for "0" 
        String resultado = "1";

        IServidor servicoCliente = null;

        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.executarTarefa(argumentos);

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim executarTarefaRemota

    public String textoAoContrarioRemoto(String host, String args) {

        IServidor servicoCliente = null;
        String resultado = "";
        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.textoAoContrario(args);

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim textoAoContrarioRemoto

    public String dataHoraServidor(String host) {
        IServidor servicoCliente = null;
        String resultado = "";
        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.dataHoraServidor();

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim dataHoraServidor

    public String contadorTarefasExecutadasRemotamente(String host) {
        IServidor servicoCliente = null;
        String resultado = "";
        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.contadorTarefasExecutadas();

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim contadorTarefasExecutadasRemotamente
}//fim classe
